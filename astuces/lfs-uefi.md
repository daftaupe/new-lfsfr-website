---
layout: astuce
use-site-title: true
title: Utilisation de Grub avec UEFI
author: Dan McGhee
translator: Julien Lepiller
date: 2014-10-16
license: GFDL-1.2
synopsis: Démarrer LFS par défaut dans un environnement UEFI avec GRUB
description: Cette astuce contient les informations utiles pour mener le gestionnaire de démarrage à utiliser le GRUB dans un environnement UEFI en  utilisant le mode EFI. Cette astuce ne s’applique qu’aux machines x86_64.
---

Prérequis
=========

*   Le système de base LFS avant ou après le Chapitre 8
*   Compréhension de base de la construction de paquets

Note
====

Les recettes dans cette astuce ne remplacent pas les instructions
des versions stables de LFS ou BLFS. Elles les améliorent seulement pour de
nouveaux matériels.  Si un conflit apparaît entre cette astuce et le livre,
merci de vous adresser à la liste de diffusion.  De plus, cette astuce ne
s’applique qu’aux machines x86_64 fournies avec Windows 7 ou Windows 8.  Les
recettes présentées ici peuvent être utilisées avec Mac OS, mais elles n’ont
pas été testés lors de l’écriture préliminaire de cette astuce.

TERMINOLOGIE
============

Ce qui suit est une terminologie des termes employés dans cette
astuce.  Plus d’informations sont disponibles dans les Références 1 à 3.

1.   _Préférences du BIOS_&nbsp;:

     Une interface au firmware à laquelle on accède par le clavier après le
     démarrage.  À l’intérieur, l’utilisateur peut changer l’ordre et la
     manière dont l’ordinateur démarre.

2.   _Système BIOS_&nbsp;:

     Firmware équipé d’un MBR

3.   _Mode EFI_&nbsp;:

     Une situation du système dans laquelle la partition EFI est montée
     et où le support des variables uefi (efi) dans le noyau fonctionne correctement.
     On y arrive en activant le mode UEFI dans les préférences du BIOS.

4.   _Point de montage EFI_&nbsp;:

     Un point de montage défini par l’utilisateur pour la
     partition EFI.  Dans cette astuce, et dans la plupart des distributions, il
     s’agit de /boot/efi.

5.   _Partition EFI_&nbsp;:

     Une petite partition, habituellement avant toutes les autres
     (c’est à dire /dev/sda1 d’une taille de 200 à 250 Mbits, formaté en FAT32 avec
     le drapeau « boot » dans parted, ou de type ef00 (EF00) dans gdisk).  Notez que
     le drapeau « boot » a une signification et un usage différent dans les disques
     partitionnés en MBR.

6.   _Variables efi (synonyme&nbsp;: variables uefi)_&nbsp;:

     des variables à l’aide desquelles le système d’exploitation peut interagir avec le
     firmware.

7.   _Option de démarrage « legacy » (« Legacy Boot »)_&nbsp;:

     un processus de démarrage dans les préférences du BIOS qui désactive le démarrage
     UEFI et utilise le CIM.

8.   _Table de partitions GUID (GPT)_&nbsp;:

     Une méthode de partitionnement qui utilise les UUID plutôt que les cylindres pour
     identifier une partition.


Discussion préliminaire
-----------------------

De plus amples informations et des discussions plus profondes des concepts suivants
peuvent être trouvés dans les Références 1 à 3.

Démarrer une LFS n’est plus aussi simple que `grub-install /dev/sda`. Il y a
plus d’alternatives et plus à examiner. Avec l’avènement et la prolifération
des firmware UEFI, la connaissance de l'utilisateur et de la philosophie du
processus de démarrage ont besoin d’être réactualisés&nbsp;:

1.   Le partitionnement GPT est différent du partitionnement MBR. L’outil
     fdisk n’est pas capable de manipuler des partitions GPT. Parted et gdisk
     (gptfdisk) sont les outils à utiliser. Chacun a ses avantages et ses
     inconvénients, ses partisans et ses détracteurs. N’importe lequel (voire
     les deux) peut être utilisé.
2.   Le firmware UEFI utilise un gestionnaire de démarrage pour choisir entre
     les chargeurs d’amorçage, comme GRUB ou LILO. Ils n’amorcent pas
     eux-mêmes la machine.
3.   Les chargeurs d’amorçage se trouvent sur la partition EFI plutôt que dans
     le MBR. Ce concept est similaire et parallèle à la manière dont LFS a
     une partition /boot séparée.
4.   Des outils supplémentaires sont requis pour que LFS puisse démarrer dans
     ce mode.
5.   LFS peut être construite et démarrée en suivant les instructions jusqu’au
     chapitre 7.6 inclu.  Pour cela, sur un firmware UEFI, les préférences
     du BIOS doivent être modifiées pour permettre un démarrage legacy.

L’une des discussions les plus abondantes autour de UEFI est le Secure Boot. Il
est nécessaire de comprendre que les termes « UEFI » et « Secure Boot » ne sont
PAS synonymes.  UEFI est un firmware.  Le Secure Boot est un procédé qui
« garanti » la sécurité et l’authenticité du chargeur d’amorçage à l’aide de
« clefs ».  NOTE&nbsp;: pour utiliser les recettes de cette astuce, il convient de
désactiver le Secure Boot dans les préférences de démarrage du BIOS.

Veuillez aussi noter que l’ordre recommandé pour utiliser ces recettes est
différent de celui présenté dans le livre.  Le plus pratique, et probablement
le plus facile, est de construire les recettes présentées ici est de construire
un système LFS jusqu’à la fin du chapitre 6. La construction de paquets
présents dans BLFS et hors de BLFS a été testé aussi bien à l’intérieur qu’à
l’extérieur de l’environnement de chroot.  Ensuite, en suivant le livre,
continuez le chapitre 7, puis revenez à ces recettes pour le chapitre 8.  Les
recettes sont présentées dans cet ordre.

Le moins pratique est d’utiliser ces recettes dans un système LFS-7.6, ou
précédent, complètement fonctionnel.  Cela implique de désinstaller grub-2.00,
de supprimer les fichiers installés par grub-install là où ils sont et de
suivre ces recettes.  Migrer du Legacy Boot au démarrage UEFI est possible.
Lors de la rédaction initiale de cette astuce cependant, cela n’a pas été
inclus.  Les Références 1 à 3 contiennent de plus amples informations à ce
sujet.

La dernière chose dont il faut se soucier est le terminal graphique de GRUB.
Dans les systèmes UEFI, si le mode vidéo de GRUB n’est pas initialisé, aucun
message de démarrage du noyau ne sera affiché jusqu’à ce que le système vidéo
du noyau ne reprenne la main.  Le paquet GRUB ne fourni aucune police et GRUB
utilise par défaut unicode.pf2.  Il y a deux manières de lui fournir cette
police.  La première consiste à copier unicode.pf2 depuis le système hôte vers
/boot/grub sur le système LFS.  La seconde consiste à configurer grub pour qu’il
construise grub-mkfont et cela ajoute une dépendance à Freetype2 pour la
construction de GRUB.  Cette astuce présente les deux possibilités.

Enfin, lors de la rédaction initiale de cette astuce, il n’y a pas de standard
pour l’utilisation de l’UEFI et l’implémentation du Secure Boot.  Tout ceci
est très dépendant du fabricant.  Cette astuce utilise la terminologie du
matériel de l’auteur initial.  Elle peut différer dans les implémentations
d’autres fabricants.  Cepedant, les opérations de mise en place du démarrage
présentés dans cette astuce restent possibles sur toutes les machines.  Les
termes peuvent différer et plus d’une opération peut être requise pour effectuer
une tâche.  Par exemple, vous pourriez avoir à désactiver le Secure Boot et
à retirer les clefs de sécurité.


Recettes
--------

{: .alert .alert-info }
__NOTE__&nbsp;:
Les recettes sont écrites dans le cas où les paquets sont construits dans
l’environnement chroot avant la fin du chapitre 8.  Elles peuvent être modifiées
sans grande difficulté pour être utilisées dans un système fonctionnel.

### Vérification du mode EFI

Avant d’entrer dans l’environnement chroot, vérifiez que l’hôte a démarré
en mode EFI.

```bash
ls /sys/firmware/efi
```

Si le répertoire existe et est rempli, le système hôte a démarré en mode
EFI.


### Monter la partition EFI

Déterminez quel périphérique correspond à la partition EFI en utilisant
gdisk ou gparted, entrez dans l’environnement chroot, créez /boot/efi au
besoin, et

```bash
mount -vt vfat /dev/sda(x) /boot/efi
```

Où `sda(x)` est le périphérique qui contient la partition EFI.

### Dépendances à la construction

Installez les paquets BLFS suivants, en utilisant les instructions du livre&nbsp;:
popt et pciutils.  Construisez et installez Freetype2 si vous construisez grub
avec le support de grub-mkfont.

#### DOSFSTOOLS (dépendance à l’exécution de efibootmgr)

Note&nbsp;: Au 3 Octobre 2014, dosfstools a été étiquetté « orphelin ». Il est
toujours fonctionnel.

*   Téléchargement&nbsp;: [http://daniel-baumann.ch/files/software/dosfstools/dosfstools-3.0.26.tar.xz](http://daniel-baumann.ch/files/software/dosfstools/dosfstools-3.0.26.tar.xz)
*   Construction et installation&nbsp;:

    ```bash
    make
    make PREFIX=/usr SBINDIR=/usr/bin MANDIR=/usr/share/man  \
    DOCDIR=/usr/share/doc install
    ```


#### EFIVAR-0.12 (dépend de popt)

Téléchargement&nbsp;:

[https://github.com/vathpela/efivar/releases/download/0.12/efivar-0.12.tar.bz2](https://github.com/vathpela/efivar/releases/download/0.12/efivar-0.12.tar.bz2)

Compilez le paquet:

```bash
sed 's|-O0|-Os|g' -i Make.defaults
sed 's|-rpath=$(TOPDIR)/src/|-rpath=$(libdir)|g' \
     -i src/test/Makefile
make libdir="/usr/lib/" bindir="/usr/bin/" \
     mandir="/usr/share/man/"     \
     includedir=/usr/include/" V=1 -j1
```

Installez le paquet&nbsp;:

```bash
make -j1 V=1 DESTDIR="${pkgdir}/" libdir="/usr/lib/" \
bindir="/usr/bin/" mandir="/usr/share/man"   \
includedir="/usr/include/" install

install -v -D -m0755 src/test/tester /usr/bin/efivar-tester
```


#### EFIBOOTMGR-0.9.0

(dépend de pciutils, efivars, de zlib à la construction et de dosfstools
pour s’exécuter.)

Téléchargement&nbsp;: [https://github.com/vathpela/efibootmgr/releases/download/efibootmgr-0.9.0/efibootmgr-0.9.0.tar.bz2](https://github.com/vathpela/efibootmgr/releases/download/efibootmgr-0.9.0/efibootmgr-0.9.0.tar.bz2)

Compilez le paquet&nbsp;:

```bash
make EXTRA_CFLAGS="-Os"
```

Installez le paquet&nbsp;:

```
install -v -D -m0755 src/efibootmgr/efibootmgr /usr/sbin/efibootmgr
install -v -D -m0644 src/man/man8/efibootmgr.8 \
          /usr/share/man/man8/efibootmgr.8
```


#### GRUB-2.02~beta2 (depend de freetype2 si vous souhaitez grub-mkfont et de efibootmgr, efivars et efivarfs à l’exécution.)

Téléchargement&nbsp;: [http://alpha.gnu.org/gnu/grub/grub-2.02~beta2.tar.xz](http://alpha.gnu.org/gnu/grub/grub-2.02~beta2.tar.xz)

Préparez la compilation:

```bash
./configure --prefix=/usr  \
   --sbindir=/sbin        \
   --sysconfdir=/etc      \
   --disable-grub-emu-usb \
   --disable-efiemu       \
   --enable-grub-mkfont   \
   --with-platform=efi    \
   --target=x86_64        \
   --program-prefix=""    \
   --with-bootdir="/boot" \
   --with-grubdir="grub" \
   --disable-werror       
```

Explication des commandes&nbsp;:

*   __--enable-grub-mkfont__  Crée une dépendance à Freetype2. Pour supprimer
    cette dépendance, n’utilisez pas cette option et copiez unicode.pf2
    de votre système hôte dans le répertoire /boot/grub de votre LFS.
    Vous pouvez aussi le télécharger sur internet.

*   __--program-prefix=""__ est simplement pratique.  Si vous ne l’utilisez
    pas, « x86_64 » est inséré dans tous les exécutables grub.  Par
    exemple, « grub-install » et « grub-mkconfig » deviennent
    x86_64-grub-install et x86_64-grub-mkconfig.

*   __--with-platform=efi__ et __--target=x86_64__ sont requis pour la
    construction en mode efi et x86_64.

Les autres options de configuration ajoutés à celles de LFS-7.6 et
LFS-SVN ont été ajouté pour s’assurer que grub sera construit et
installé dans les répertoires utilisés par cette astuce.  Ils peuvent
être utilisés ou éliminés suivant votre convenance.


Compilez le paquet&nbsp;:

```bash
make
```

Installez le paquet:

```bash
make install
```


### LFS Chapitre 7:

Lorsque vous construisez le fichier `/etc/fstab`, ajoutez les lignes
suivantes&nbsp;:

```
/dev/sda(x)     /boot/efi    vfat     defaults            0     1

efivarfs       /sys/firmware/efi/efivars  efivarfs  defaults  0      1
```

Où `/dev/sda(x)` est la partition EFI.


### LFS Chapitre 8:

#### Options de configuration du noyau pour EFI

```
   CONFIG_EFI_PARTITION=y
   CONFIG_EFI=y
   CONFIG_EFI_STUB=y
   (Ce qui permet de démarrer le noyau depuis la partition EFI si besoin)
   CONFIG_FB_EFI=y
   CONFIG_FRAMEBUFFER_CONSOLE=y
   # CONFIG_EFI_VARS is not set
N’activez pas cette option.  Elle crée des conflits à l’exécution avec EFIVARFS qui la replace.
   CONFIG_EFIVAR_FS=y
   # CONFIG_UEFI_CPER is not set
   # CONFIG_EARLY_PRINTK_EFI is not set
```

Le module EFI_VARS sera bientôt déprécié et inusité sauf dans de vieux
noyaux.

Les deux dernières options sont incluses dans le noyau 3.13.3 et peuvent
être activés en fonction de vos préférences et des besoins du système.


#### Utiliser Grub pour mettre en place le processus de démarrage

Si grub a été construit sans grub-mkfont et que unicode.pf2 est dans
`/boot/grub`, sautez cette section.

Sinon&nbsp;:

Téléchargez et installez unifont-7.0.05&nbsp;:

```bash
wget http://unifoundry.com/pub/unifont-7.0.05/font-builds/unifont-7.0.05.pcf.gz
mkdir -pv /usr/share/fonts/unifont
gunzip -c  unifont-7.0.05.pcf.gz &gt; /usr/share/fonts/unifont/unifont.pcf
grub-mkfont -o /usr/share/grub/unicode.pf2 \
      /usr/share/fonts/unifont/unifont.pcf
```


#### Installer Grub sur la partition EFI

Installer grub sur la partition EFI et créer une entrée dans le gestionnaire
de démarrage est la grande différence entre les recettes de cette astuce et
la procédure du livre.  Techniquement, ce n’est pas une divergence par rapport
aux concepts du livre.  Ces instructions installent GRUB sur le MBR, la
partie protégée d’un disque GPT ou sur une partition /boot dédiée.  Les
présentes recettes installent GRUB sur la partition EFI et génèrent une
entrée dans le gestionnaire de démarrage du système.  C’est pour cette seule
commande que cette astuce a été écrite et que les paquets hors de LFS ont
été installés.

```bash
grub-install --target=x86_64-efi --efi-directory=/boot/efi  \
   --bootloader-id=LFS --recheck --debug
```

Avec&nbsp;:

*   __--efi-directory=&lt;Point de montage EFI&gt;__ pas la partition EFI elle-même
*   __--bootloader-id=&lt;un nom&gt;__ est le répertoire sur la partition EFI dans lequel
    l’image GRUB est écrite.

Cette commande génère beaucoup de sortie.  Mais à la fin, elle indiquera si
elle a fonctionné.  Cette commande installe l’image GRUB dans
`/boot/efi/EFI/LFS/grubx64.efi` et crée l’entrée « LFS » dans le gestionnaire
de démarrage du système.

Pour le vérifier, inspectez le contenu de `/boot/efi/LFS` et, en root, lancez
&lt;efibootmgr&gt;.  Les résultats de ces commandes listent l'ordre de démarrage
et toutes les entrées de démarrage.  Si l’entrée « LFS » n’apparaît pas, lisez
la page de manuel de efibootmgr, créez une entrée et changez l’ordre de
démarrage comme vous le souhaitez.

#### Configuration du Grub

Générez grub.cfg:

```bash
cat > /boot/grub/grub.cfg << "EOF"
# Begin /boot/grub/grub.cfg
set default=0
set timeout=5

insmod ext2
set root=(hd[x], gpt[y])
# hd[x] is the drive of the LFS partion and gpt[y] is the partition

insmod efi_gop
insmod efi_uga
insmod font
if loadfont /grub/unicode.pf2; then
  loadfont /grub/unicode.pf2
  set gfxmode=auto
  insmod gfxterm
  set gfxpayload=keep
  terminal_output gfxterm
fi

menuentry "GNU/Linux, Linux <kernel name>"  {
  linux   /boot/vmlinuz-<kernel name>; root=/dev/sda[x] ro
}  
EOF
```

Notez que dans « menuentry », `/dev/sda[x]` est le périphérique de la partition
LFS.

Discussion finale
=================

Comme nous l’avons signalé, l’implémentation du firmware UEFI et sa manipulation
dépend grandement du fabricant.  Lors de l’écriture initiale de cette astuce,
il n’y a pas d’approche standard.  Ainsi, alors que les recettes présentées
agissent comme prévu, le système peut malheureusement ne pas choisir de lui-même
d’utiliser le chargeur de démarrage grub par défaut.  Dans ce cas, relisez les
Références 1 à 3, elles fournissent des informations qui vous conduiront à une
solution pour votre situation.  Comme toujours, parmi les meilleures ressources
se trouvent les listes de diffusion de {,B}LFS.

Maintenant, il est utile de préciser qu’il y a d’autres outils utiles&nbsp;:
gummiboot et rEFInd en sont deux exemples.  Ils sont décrit comme des
gestionnaires de démarrage, mais sont en fait une couche en espace utilisateur
entre le gestionnaire de démarrage d’OS et le chargeur d’amorçage.  Des
informations sur les deux sont dans les références.

Références
==========

1.  __Rod’s Books__  Une collection d’articles sur le net qui détaillent en
    profondeur les concepts du démarrage UEFI, du partitionnement et des outils.
    L’URL suivante dirige directement vers les informations sur EFI.
    www.rodsbooks.com est la page principale et présente plein, plein de bons
    articles.
  URL:  [http://www.rodsbooks.com/efi-bootloaders/index.html](http://www.rodsbooks.com/efi-bootloaders/index.html)

2.   __"Unified Extensible Firmware Interface-ArchWiki"__
     URL:  [https://wiki.archlinux.org/index.php/Unified_Extensible_Firmware_Interface](https://wiki.archlinux.org/index.php/Unified_Extensible_Firmware_Interface)

3.  __"GRUB-ArchWiki"__
    URL:  [https://wiki.archlinux.org/index.php/GRUB](https://wiki.archlinux.org/index.php/GRUB)

4.  __Google__


Remerciements
=============
*   Craig Magee &lt;lfs-support at lists.linuxfromscratch.org&gt; pour ses
    commentaires et ses tests,
*   [Pierre Labastie](http://lists.linuxfromscratch.org/listinfo/lfs-dev) pour
    ses tests, la manipulation des polices et ses commentaires.

TODO
====

*   Ajouter les numéros de paragraphe et de section et une table des matières
    pour permettre la recherche.
*   Ajouter un appendice pour la migration du mode Legacy au mode UEFI.
*   Ajouter un appendice pour plus de manières d’arriver à démarrer sur grub par
    défaut.
*   Ajouter un appendice pour LVM.
*   Ajouter un appendice pour un grub « standalone » sur la partition EFI
    indépendant de la distribution.

CHANGELOG
=========

[TBD]
*   Astuce initiale.
