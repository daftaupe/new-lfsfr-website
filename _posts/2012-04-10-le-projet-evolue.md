---
layout: post
title: Le projet évolue
date: 2012-04-10
tags: BLFS
---

Comme vous le savez l'équipe de traduction a enfin réussi à se synchroniser sur blfs VO et nous parvenons, grâce au gros travail de Denis, à rester synchronisés en permanence avec le projet anglais.

Ce projet a changé sa politique de publication concernant BLFS. En effet, face à l'énormité du livre qui le rend ingérable pour une si petite équipe, il a été décidé de ne plus publier de version stable. L'utilisateur aura, par contre, des repères sur la fiabilité des instructions. Ainsi, pour chaque paquet, les auteurs indiquent avec quoi la construction est censée marcher, c'est-à-dire sur quelle version du livre LFS il se base. L'équipe considère par ailleurs que, même si certains paquets ne sont pas à jour, l'utilisateur ne se met pas en danger en construisant une version plus récente que celle couverte par les instructions. Les modifications éventuelles des commandes restent marginales. Contrairement à LFS, il n'est pas obligatoire d'être aussi rigoureux.

Dans ce cadre, BLFS propose donc la lecture en ligne du livre dans sa dernière publication, mais aussi le téléchargement de l'ouvrage. L'équipe de traduction fait de même. Désormais, vous pouvez télécharger une version html multifichiers du livre blfs, mise à jour quotidiennement selon les modifications ayant eu lieu dans la traduction.

Nous espérons ainsi synchroniser les apports de la version anglaise et de celle française de BLFS avec un service équivalent.

Bon téléchargement!

Cordialement,