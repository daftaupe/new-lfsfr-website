---
layout: post
title: LFS 10.0 est disponible
date: 2020-09-01
tags: LFS
---

Nous sommes heureux de vous annoncer la sortie de LFS 10.0 conjointement en
version SysV et Systemd, ainsi que la sortie de BLFS 10.0 dans ces deux
versions.

Cette nouvelle version est une version majeure qui fait suite à la
réorganisation complète de la méthode de construction de LFS. Le livre
utilise désormais une technique de compilation croisée améliorée et un
environnement isolé du système hôte pour construire les outils avant le
système final. Cela réduit à la fois les chances de modifier le système
hôte par inadvertance et l'influence du système sur la le processus de
cosntruction présenté dans LFS.

Cette nouvelle version contient une mise à jour majeure de la chaîne d’outils
avec les versions glibc-2.32, binutils-2.35 et gcc-10.2.0. Au total, ce sont 37
paquets qui ont été mis à jour. Un énorme travail rédactionnel a été réalisé
pour moderniser les textes tout au long du livre. Enfin, le noyau Linux a été
mis à jour vers la version 5.8.3.

Un grand merci à celles et ceux qui ont contribué à la traduction et aux
relectures !

N'hésitez pas à lire et télécharger le livre sur le site
[fr.linuxfromscratch.org](//fr.linuxfromscratch.org) aux formats html, pdf ou
epub, puis à nous adresser vos retours sur la liste de diffusion, le canal IRC
ou [mastodon](https://mamot.fr/@lfsfr).

Bonne lecture !
