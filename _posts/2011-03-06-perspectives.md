---
layout: post
title: Perspectives
date: 2011-03-06
tags: CLFS
---

La progression de blfs nous permet de bouger un peu le curseur pour le projet clfs. Plusieurs perspectives s'offrent à nous. D'abord, finir de traduire le livre expliquant comment créer un système multilib. Ensuite, traduire d'autres architectures (sparc, ppc, arch...). Ceci devrait être moins colossal si on considère qu'il ne reste que les parties spécifiques à traduire. Ensuite, une relecture n'est pas inutile.

Enfin, de nouveaux objectifs peuvent émerger au stade de plus long terme, notamment celui de traduire le livre traitant des systèmes embarqués ou d'archi très spécifiques.

Ces objectifs sont nouveaux. S'ils vous intéressent n'hésitez pas à les rejoindre. Le dynamisme du projet dépend de vous.

Plus d'infos sur Participez.