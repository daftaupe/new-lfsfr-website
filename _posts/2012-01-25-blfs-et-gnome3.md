---
layout: post
title: BLFS et Gnome3
date: 2012-01-25
tags: BLFS
---

Comme vous le savez sûrement, Gnome3 représente une innovation telle qu'il implique un travail considérable dans les différentes distributions qui souhaitent l'intégrer. Gnome2 était déjà basé sur au moins 200 paquets; Gnome3 implique de tout revoir.

BLFS s'est quand même lancée dans l'aventure. Mais les développeurs sont conscients que, dans les premiers moments, la mise à jour empêchera la construction de Gnome. Or, bien qu'en version de développement, BLFS svn reste une référence puisque la dernière version stable publiée date de plusieurs années.

Pour ne pas pénaliser les lecteurs réguliers du livre, sans pour autant bloquer sa mise à jour, un des développeurs a figé une version relativement stable en date du 24 janvier (ou du 20 janvier précisément, selon le general.ent). Cette version, non officielle et non stable, contient les instructions pour Gnome2 et permet donc de construire un bureau Gnome, en attendant que la construction de Gnome3 rçussisse. Elle est transitoire.

L'équipe de traduction de BLFS souhaite offrir la même possibilité aux lecteurs de la traduction. C'est pourquoi, à l'instar des anglophones, nous mettons à votre disposition la traduction de la version du 20 janvier, dernière avant Gnome3.

Cette version se trouve ici.

Vous pouvez la télécharger
ici.

Cette version est transitoire en attendant que gnome3 se construise correctement.

N'hésitez pas à nous solliciter pour toute difficulté ou à nous signaler un problème dans cette version qui restera à relire pour longtemps.

NB: pour le dépôt français, la révision est la 1558. La 1560 passe déjà à gnome3.

Cordialement,