---
layout: post
title: LFS 8.2 est disponible
date: 2018-03-04
tags: LFS
---

Nous sommes heureux de vous annoncer la sortie de LFS 8.2 conjointement en version SysV et Systemd, ainsi que la sortie de BLFS 8.2 dans ces deux versions.

Cette nouvelle version contient une mise à jour majeure de la chaîne d’outils avec les versions glibc-2.27, binutils-2.30 et gcc-7.3.0. En plus de cela, 5 paquets ont été déplacés depuis BLFS vers le livre LFS de base. Il s'agit de libffi, openssl, python3, ninja et meson. Un énorme travail rédactionnel a été réalisé pour améliorer les textes tout au long du livre. Enfin, le noyau Linux a été mis à jour vers la version 4.15.3.

Un grand merci à celles et ceux qui ont contribué à la traduction et aux relectures !

N'hésitez pas à lire et télécharger le livre sur le site [fr.linuxfromscratch.org](//fr.linuxfromscratch.org) aux formats html ou pdf, puis à nous adresser vos retours sur la liste de diffusion, le forum ou le canal IRC.
