---
layout: post
title: LFS progresse
date: 2008-10-04
tags: LFS
---

Hier est sortie une nouvelle version SVN de LFS. Sa principale évolution, à ce jour, est la version de GCC, exigeant l'installation de deux nouveaux paquets: gmp et MPFR.

Cette mise à jour a été traduite et placée dans LFS-SVN1003. Mais des débats subsistent sur certains points: la glibc pourrait bientôt être également mise à jour, des correctifs pourraient intervenir... Donc, pour laisser une version SVN (récente) stable, l'équipe de traduction a décidé de laisser en ligne jusqu'à stabilisation la version LFS-SVN-20080711. Par contre, le code xml de cette version n'est pas maintenu, on ne peut garder toutes les versions xml des SVN. La SVN-20080711 disparaîtra du site dès la stabilisation de la SVN-20081003 anglaise.

D'ici là, la version traduite suivra les évolutions anglaises, qui pourraient se multiplier dans les prochains jours.