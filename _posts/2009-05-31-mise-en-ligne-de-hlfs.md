---
layout: post
title: Mise en ligne de hlfs
date: 2009-05-31
tags: HLFS
---

Après près d'un an de travail, nous sommes ravis de pouvoir vous annoncer la sortie de la traduction des livres de hlfs, dans leur version de développement qui est la seule disponible, y compris en anglais.

Nous tenons à remercier tout particulièrement le relecteur du livre qui a fourni un travail considérable, par sa qualité comme par sa quantité. Nous attendons à présent les évolutions de la version originale, sereinement.

En attendant, bonne lecture de ces ouvrages, très intéressants pour ceux qui se préoccupent de la sécurité en informatique et qui veulent mieux comprendre le fonctionnement d'un système Linux paramétré selon de telles exigences. Il est disponible en lecture et en téléchargement au format html.

Merci encore à notre relecteur et amusez-vous bien.