---
layout: post
title: LFS 8.0 est disponible
date: 2017-02-26
tags: LFS
---

Nous sommes heureux de vous annoncer la publication de la traduction de LFS version 8.0, dans ses versions SysV et Systemd. Cette version, sortie moins d'un jour après la version anglaise, contient une mise à jour majeure de la chaîne d'outils avec les versions glibc-2.24, binutils-2.27 et gcc-6.2.0. Au total, 29 paquets ont été mis à jour, les scripts de démarrage ont été corrigés et un énorme travail rédactionnel a été réalisé pour améliorer les textes tout au long du livre.

En plus des mises à jours traditionnelles, le livre est publié dans une nouvelle version majeure car le lien symbolique entre /lib64 et /lib a été abandonné, et le dossier /lib64 n'est plus du tout utilisé. De plus, l'éditeur de lien gold est désormais disponible, mais n'est pas utilisé par défaut.

Un grand merci à celles et ceux qui ont contribué à la traduction, en particulier à Alexandre, Amaury, Denis et Jean-Philippe.

N'hésitez pas à lire et télécharger le livre sur le site www.fr.linuxfromscratch.org aux formats html ou pdf, puis à nous adresser vos retours sur la liste de diffusion, le forum ou le canal IRC.
