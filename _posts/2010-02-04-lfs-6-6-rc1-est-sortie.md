---
layout: post
title: LFS 6.6-rc1 est sortie
date: 2010-02-04
tags: LFS
---

L'équipe anglophone de LFS vient d'annoncer la version 6.6-rc1 de LFS en anglais. Elle met à jour des applications et constitue une importante mise à niveau pour les systèmes 64 bits.

rc1 signifie "release candidate". Cela signifie que c'est une avant-version définitive mais elle est stable. Toutefois, la gestion des modifications à venir de cette version pourrait s'avérer pénible, car les anglophones opèrent les modifications dans cette version et les reportent dans la version de développement. Pendant quelques semaines, ils gèrent donc simultanément deux versions et font les changements en double.

L'équipe de traduction choisit une autre méthode, car maintenir ce double travail nous paraît fastidieux. Aussi, les utilisateurs doivent savoir que, jusqu'à la version 6.6 définitive, la version de développement peut être considérée comme stable. Elle représente la 6.6-rc1.

Cela signifie que ceux qui veulent déjà lire la mise à jour de lfs-6.5 (qui est 6.6-rc1), vous pouvez le faire en lisant la traduction de la version de développement qui a le même contenu. Dès que la 6.6 sortira, nous retournerons sur la situation normale: 6.6 stable et version de développement potentiellement instable.

N'hésitez pas à poser vos questions et réagir sur notre liste de diffusion ou notre forum, voire le canal irc. D'ici un mois, la 6.6 arrive!