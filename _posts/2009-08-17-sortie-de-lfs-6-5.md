---
layout: post
title: Sortie de LFS-6.5
date: 2009-08-17
tags: LFS
---

L'équipe anglophone de Linux From Scratch est heureuse d'annoncer la sortie de LFS version 6.5. Cette version comprend de nombreuses modifications par rapport à LFS-6.4 (notamment des mises à jour vers Linux-2.6.30.2, GCC-4.4.1 et Glibc-2.10.1) et des corrections de sécurité. Elle inclut également un travail éditorial sur les explications de fond contenues dans le livre, améliorant tant la clarté que le soin apporté au texte.

Vous pouvez lire la traduction du livre en ligne ici ou la télécharger ici, pour l'instant en html, bientôt en pdf.

N'hésitez pas à adresser vos commentaires concernant cette traduction sur cette liste, ou au sujet du livre à la liste de diffusion de l'équipe de développement (en anglais).

Bonne lecture,