---
layout: post
title: Le site fait peau neuve
date: 2015-01-26
tags: MAIN
---

Un nouveau petit changement vient d'intervenir dans l'esthétique du site.

Petit point historique: la première mouture du site date de 2004, dans une version statique HTML. En 2008, le site est passé dans une version dynamique et a tenté de rapprocher son aspect du site anglais du projet. En 2014, il a migré pour être hébergé directement sur les serveurs du projet d'origine. Aujourd'hui, un nouveau cap est franchi dans l'homogénéité avec une revue esthétique du site. Elle vise à se rapprocher le plus possible de l'aspect du site anglais.

Une prochaine modification qui va bientôt intervenir pourrait toucher le texte de la page d'accueil, de sorte que des  liens soient ajoutés vers chaque projet géré par LFS. Cette évolution fais suite à la demande de visiteurs de plus de clarté, ceux-ci ne s'y retrouvant pas suffisamment dans le bandeau horizontal en haut du site où il n'apparaitrait pas clairement la possibilité de cliquer sur chaque sous-projet.

Ces modifications sont donc à venir mais d'ores et déjà, vous pouvez constater celles opérées d'un point de vue purement esthétique sur le site. N'hésitez pas à réagir, notamment sur la liste de diffusion.