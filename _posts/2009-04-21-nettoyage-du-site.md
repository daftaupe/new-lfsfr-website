---
layout: post
title: Nettoyage du site
date: 2009-04-21
tags: MAIN
---

Le projet de traduction de LFS a progressé depuis quelque temps. La traduction de clfs est terminée et n'attend qu'une relecture, des astuces ont été traduites, le livre HLFS est en cours de relecture... Merci infiniment à ceux qui ont accepté de nous aider, leur soutien nous fait du bien.

Du coup, le site vient d'être réactualisé. Les informations expliquant comment participer, les ouvrages en lecture et en téléchargement et les compositions des différentes équipes ont été mises à jour et nettoyées. N'hésitez pas à visiter chaque projet pour y observer les évolutions. Il y a du tout frais tout neuf.

Bonne visite.