---
layout: post
title: La collection automne de LFS, volet 1
date: 2015-10-04
tags: LFS
---

Comme en chaque début d'automne, nous avons le plaisir de vous annoncer une nouvelle version de Linux From Scratch. Celle-ci est d'ores et déjà traduite en français dans sa version systemd. La version classique sysvinit sera disponible en français très prochainement.

À cette occasion, l'équipe francophone a décidé de changer l'apparence du livre, avec une nouvelle barre de navigation à gauche, pour une meilleure lecture en situation de mobilité. Vos avis sont les bienvenus.

Hormis l'apparence et l'intégration des dernières versions de paquets, le principal changement est la suppression de toutes les bibliothèques statiques qui ne sont plus nécessaires dans un système GNU/Linux moderne. Par ailleurs, cela réduit la taille d'un grand nombre de paquets.

Linux From Scratch: Your Distro, Your Rules (votre distrib', vos règles)