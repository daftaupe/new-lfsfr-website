---
layout: post
title: Une première étape dans la traduction de CLFS
date: 2008-12-31
tags: CLFS
---

L'équipe de traduction de LFS est ravie d'annoncer la fin du premier jet de la traduction de CLFS, version 1.1 stable, applicable à la construction d'un système pur 64 bits. Pour cet ouvrage, reste à présent à déboguer le xml, ce que nous sommes en train de faire et vous pouvez y contribuer si désiré; puis à relire la traduction.

La traduction du livre pour le multilib devrait prendre moins de temps car beaucoup d'éléments sont communs à tout le livre et ont déjà été traduits.

À moyen terme, la traduction de la version SVN anglaise du livre sera effectuée. À un peu plus long terme, nous réfléchissons à élargir la traduction aux autres architectures proposées. Mais ce n'est pas encore décidé.

N'hésitez pas en tout cas à nous aider et donner votre avis.