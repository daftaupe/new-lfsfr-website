---
layout: post
title: Achèvement de la traduction de BLFS
date: 2011-07-15
tags: BLFS
---

Après deux ans de travail, et grâce au soutien exceptionnellement dense et constant d'un contributeur récent, l'équipe lfs-fr a l'immense plaisir de vous annoncer la sortie de la traduction du livre BLFS, version de développement. Cette traduction permettra d'être un document de référence moins obsolète que blfs 6.3, que nous n'avons par conséquent pas traduit vu qu'elle est obsolète. Les prochaines versions du livre LFS se référeront à la traduction et non au livre BLFS anglais.

Compte tenu de la masse de l'ouvrage, nous avons fait une dérogation à notre fonctionnement habituel. La publication intervient sans relecture. Les utilisateurs seront nos relecteurs, sauf si quelqu'un veut s'y ateler à temps plein, au quel cas il pourra relire et corriger. N'hésitez donc pas.

Bonne lecture