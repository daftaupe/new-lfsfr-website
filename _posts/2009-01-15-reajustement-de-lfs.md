---
layout: post
title: Réajustement de lfs
date: 2009-01-15
tags: LFS
---

Suite à des messages de nombreux utilisateurs sur le forum, il a été décidé de nous lancer dans une grande entreprise de relecture du livre lfs 6.4. L'accent a été mis sur les commandes, qui ne peuvent pas se permettre d'être erronnées.

Ce travail ne fut pas inutile. Il a permis de dénicher de nombreuses fautes que l'équipe lfs-fr ne s'explique guère. En tout cas, elles  sont toutes corrigées. La version française de lfs-6.4 est donc à présent parfaitement conforme à la version anglaise. Subsistent peut-être des fautes de frappe dans le texte mais plus dans les commandes, ce qui est le plus important.

Merci beaucoup aux utilisateurs pour leur signalement et désolé pour ces petits défauts  inattendus. Nous entamons et finirons très prochainement la même opération pour la traduction de la version svn anglaise.

N'hésitez pas bien entendu à nous faire partager votre expérience. Si vous voyez des fautes  dans le texte ou dans un script ou une commande de la traduction de la svn anglaise, signalez-le nous. D'avance merci.