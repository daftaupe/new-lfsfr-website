---
layout: post
title: Sondage sur le look des livres
date: 2015-07-18
tags: MAIN
---

Un de nos membres a inspiré à notre équipe une réflexion sur l'aspect des livres, en particulier leur facilité de lecture sur des appareils mobiles ou de petits écrans. Cette réflexion a débouché sur [cette proposition de mise en page](http://www.fr.linuxfromscratch.org/view/newcss/blfs-7.7-fr/postlfs/emacs.html).

Nous soumettons à notre communauté la question de savoir si elle préfère l'ancien style, ou le nouveau. Si le nouveau suscite l'adhésion, nous le proposerons à l'équipe du livre originel. Pour [participer au sondage, cliquez ici](https://framadate.org/4irggtform25rcuu).

Les sondages sont ouverts jusqu'à fin juillet, n'hésitez pas!