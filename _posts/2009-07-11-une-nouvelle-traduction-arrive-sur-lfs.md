---
layout: post
title: Une nouvelle traduction arrive sur lfs
date: 2009-07-11
tags: MAIN
---

Nous observons depuis quelques mois que ce site est visité en moyenne par 300 à 400 personnes par jour. Ce chiffre nous motive, car il montre que notre travail est observé et peut aider des gens. Il est donc très encourageant.

L'appui d'un nouveau relecteur depuis quelques mois nous permet par ailleurs d'avancer très vite, efficacement et dans un état d'esprit excellent. Nous le remercions infiniment pour tout son travail de traduction d'astuces et de relecture. Il aura permis en quelques mois de presque boucler les chantiers ouverts pour hlfs et clfs et il améliore régulièrement les livres traduits, comme par exemple lfs, ce qui est considérable.

Il est par ailleurs à l'origine d'un errata pour le livre hlfs qui actualise les liens de téléchargement des paquets. Cet errata non officiel a amené l'équipe de traduction à s'interroger sur l'intérêt de traduire les errata fournis par les anglophones parallèlement aux livres traduits sur ce site. Très vite, il est apparu important. D'autant qu'il permet d'actualiser les livres traduits, complétant les projets en voie d'achèvement.

Du coup, l'équipe de traduction a décidé de traduire, progressivement, les errata des livres disponibles sur le site anglophone. Le lecteur aura ainsi à sa disposition, dans la rubrique Lecture de chaque projet, la traduction de l'errata officiel et, si nécessaire, un errata non officiel (comme pour hlfs). L'errata étant un fichier texte, il pourra même facilement le télécharger. Bien que ce soit un nouveau chantier, il est assez petit car les errata ne sont pas très longs. L'objectif est de les traduire à moyen terme.

Cette évolution permettra d'aider le lecteur du livre francophone et de mettre à sa disposition les mêmes éléments que ceux dont dispose celui anglophone, dans un cadre de plus en plus intégralement traduit. Nous espérons que cela servira et avons la conviction que cela peut aider à promouvoir la démarche LFS.