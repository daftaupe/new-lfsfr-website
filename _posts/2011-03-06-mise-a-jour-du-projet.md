---
layout: post
title: Mise à jour du projet
date: 2011-03-06
tags: HLFS
---

La finalisation du premier jet de la traduction de blfs permet de jeter un regard de nouveau plus attentif sur hlfs. Or, son stade est toujours celui du développement. Néanmoins, il est possible de traduire les fichiers txt désormais utilisés, pour lesquels une traduction a déjà commencé.

C'est la mise à jour de ces traductions que nous allons entamer et tenter à la fois de conclure et de maintenir synchronisée. N'hésitez pas à nous y aider et à relire ce qui est fait. Plus d'infos sur la page Participez.