---
layout: post
title: Sortie de LFS 6.7
date: 2010-09-19
tags: LFS
---

L'équipe de LFS-fr est heureuse de vous annoncer la publication de LFS version 6.7. Cette version contient de nombreux changements par rapport à celle 6.6, notamment s'agissant des versions de logiciels aussi importants que Binutils, le noyau Linux,, GCC et Glibc. Vous y trouverez aussi des corrections de sécurité.

Un travail rédactionnel a également été accompli sur les explications contenues dans l'ouvrage, qui a cherché à la fois à les clarifier et à les améliorer.

Nous tenions à remercier tous ceux qui ont participé à la traduction du livre. Leur soutien nous a été précieux et leur fidélité est particulièrement motivante.

N'hésitez pas à lire et télécharger le livre sur ce site aux formats html ou pdf, puis à nous adresser vos retours sur la liste de diffusion, le forum ou le canal IRC.