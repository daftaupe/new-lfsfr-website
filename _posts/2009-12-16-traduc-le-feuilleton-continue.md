---
layout: post
title: Traduc, le feuilleton continue
date: 2009-12-16
tags: MAIN
---

Ce soir Traduc vient de réélire un nouveau Conseil d'Administration. Grâce à une communication intense sur sa liste, l'association a pu se doter d'un Conseil d'Administration. Suffisamment de candidats se sont présentés, si bien que l'organe s'en trouve même plus élargi qu'avant. Sa survie à moyen terme n'est donc plus menacée.

Tout n'est pas résolu, il reste à lui donner tout son dynamisme. Telle sera la tâche du nouveau Conseil d'Administration qui va prochainement élire son bureau et, notamment, un nouveau président. Le projet lfs-fr est soulagé de cette nouvelle, d'abord car la disparition d'une association comme traduc eut été très triste, elle représente tout un état d'esprit et un historique; ensuite car c'est l'hébergeur du projet.

Nous sommes donc à présent rassurés. Reste à espérer que cette situation difficile ne se reproduira plus et que le dynamisme reprendra sa vigueur.