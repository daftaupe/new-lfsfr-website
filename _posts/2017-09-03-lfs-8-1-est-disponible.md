---
layout: post
title: LFS 8.1 est disponible
date: 2017-09-03
tags: LFS
---

Nous sommes heureux de vous annoncer la sortie de LFS 8.1 conjointement en version SysV et Systemd, ainsi que la sortie de BLFS 8.1 dans ces deux versions.

Cette nouvelle version contient une mise à jour majeure de la chaîne d’outils avec les versions glibc-2.26, binutils-2.29 et gcc-7.2.0. Au total, 32 paquets ont été mis à jour, les scripts de démarrage ont été mis à jour et un énorme travail rédactionnel a été réalisé pour améliorer les textes tout au long du livre.

Un grand merci à celles et ceux qui ont contribué à la traduction, en particulier à Alexandre, Amaury, Denis, Stanislas et Ygnobl.

N'hésitez pas à lire et télécharger le livre sur le site www.fr.linuxfromscratch.org aux formats html ou pdf, puis à nous adresser vos retours sur la liste de diffusion, le forum ou le canal IRC.
