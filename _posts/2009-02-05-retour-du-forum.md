---
layout: post
title: Retour du forum
date: 2009-02-05
tags: MAIN
---

Nous avons la joie de vous annoncer le retour du forum sur Absolinux. Il est opérationnel depuis quelques instants comme promis. Vous pouvez donc poser toutes vos questions dessus sans difficulté.

Signalons que la société 1and1 a débloqué l'accès au site après avoir fait une recherche et découvert que les spam émanaient d'une attaque et non d'une volonté du développeur d'absolinux. Elle a donc quelque peu atténué sa position d'il y a peu. Il aura fallu un mail de la part d'absolinux et près de 72 heures d'indisponibilité. Nous pouvons à présent, pour diverses raisons dont celles expliquées hier, espérer qu'un tel événement ne se reproduira plus et que notre forum sera accessible en continu, les futures éventuelles migrations étant transparentes.

Désolé du dérangement. N'hésitez pas à nous  solliciter et nous transmettre vos besoins.