---
layout: post
title: Souci sur le forum
date: 2009-01-24
tags: MAIN
---

Pour des raisons techniques indépendantes  de notre volonté, le forum est actuellement indisponible. Nous nous efforçons de résoudre  l'incident le plus vite possible. Notre hébergeur semble avoir été victime d'un piratage  dont il est en train de mesurer et de réparer les conséquences.

N'hésitez pas en attendant à nous poser vos questions sur la liste de diffusion ou l'irc comme indiqué dans la rubrique Support.

Désolé des inconvénients qui en résultent et dont nous ne nous passerions bien. Le piratage fait partie des aléas d'un projet.