---
layout: post
title: La gouvernance de LFS-FR évolue
date: 2014-03-07
tags: MAIN
---

Depuis ses origines, le projet de traduction Linux From Scratch fonctionne sur une base très ouverte mais qui peut apparaître comme segmentée. On y a un coordinateur général du projet et de la plupart des livres et, pour certains livres comme BLFS, un coordinateur de la traduction d'un ouvrage. Si cette façon de travailler a amené le projet à de très bons résultats, la diversification des livres, des contributeurs et le temps nous amènent à réfléchir à la façon dont nous fonctionnons. En apparence, on peut voir les choses comme cloisonnées même si en pratique, elles ne le sont pas. Enfin, du point de vue du contributeur, la seule porte d'entrée avec un projet était une personne.

Cette situation a plusieurs inconvénients. Le premier est qu'elle oblige la personne à afficher son adresse de messagerie, or elle n'y tient pas forcément. Le second est que, si elle doit se mettre en retrait, la passation de témoin est plus lourde. Le troisième est que tout repose sur elle, alors qu'en pratique elle travaille étroitement avec le coordinateur francophone général. Ce dernier assure la traduction, relecture et représente l'interface officielle (mais non exclusive) entre le projet et l'association qui l'héberge (Traduc.org), entre le projet et les auteurs en amont, et entre le projet et l'extérieur.

Son rôle n'a pas vocation à changer, car il est important qu'il y ait une personne référente même si chacun peut s'exprimer pour le projet qu'il suit de plus près que lui. La question se pose de savoir comment faire en sorte de garantir la pérennité de la coordination de chque livre et une certaine fluidité.

Pour répondre à tous ces impératifs, la rubrique L'équipe de chaque projet mentionne toujours qui est le coordinateur de la traduction d'un livre. Là où cela change, c'est pour le contacter. Au lieu de le contacter personnellement, les contributeurs sont désormais invités à contacter la liste de diffusion. Cela l'anime, mais cela lui permet aussi de jouer pleinement son rôle communautaire. Dès qu'une personne la contacte, bien sûr, le coordinateur pour un livre donné réagit prioritairement. Mais en cas de difficulté, tout le monde est informé et peut ainsi réagir, voire agir.

Ainsi, tout en ayant un individu identifié, ce n'est pas sur lui que tout repose et la confidentialité de son adresse de messagerie, parfois privée, est respectée. Tout en restant au premier plan, le coordinateur est soutenu par toute l'équipe qui peut pallier une absence temporaire, répondre aux sollicitations et l'aider dans sa tâche.

C'est en ce sens que le site Web vient d'être modifié. Le contact par défaut est désormais la liste de diffusion afin de sensibiliser toute l'équipe aux sollicitations et d'alléger la charge pour le coordinateur. De cette façon, nous espérons nous ouvrir encore un peu plus et assurer la pérennité du projet à long terme en englobant de nouvelles contributions avec une souplesse maximale.