---
layout: post
title: Un errata maison de LFS 6.8
date: 2011-07-26
tags: LFS
---

Un des problèmes réguliers et récurrents que rencontrent les utilisateurs de LFS tient à l'obsolescence des adresses des paquets dont ils ont besoin. Ce problème, inexistant le lendemain de la sortie du livre, s'accroît de plus en plus au fur et à mesure que la version stable vieillit. Il peut frapper la version de développement, mais plus rarement.

Il existe, selon les auteurs du livre, plusieurs moyens de faire face au problème: chercher sur Google, ou espérer que d'autres utilisateurs ont récupéré les sources à temps, les ont archivées et mis en ligne sur leur serveur. Le livre mentionne une adresse référençant ce type d'initiative.

Un utilisateur francophone vient d'envoyer sur le forum son propre wget-list, avec toutes les adresses actualisées. Nous le remercions. Ce fichier a été mis en ligne sous forme d'un errata non officiel, puisqu'il émane de la communauté française et non des rédacteurs de LFS. Vous le trouverez dans les rubriques pour lire et télécharger la version stable sur ce site.

Nous pensons que cela enlève un ennui récurrent des utilisateurs. Aussi nous en félicitons-nous.