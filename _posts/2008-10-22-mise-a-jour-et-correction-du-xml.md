---
layout: post
title: Mise à jour et correction du XML
date: 2008-10-22
tags: LFS
---

Comme prévu, LFS a légèrement évolué et de futurs petits mouvements sont en préparation. Pour l'heure nous avons mis en ligne la SVN du 20 octobre, tout comme est en ligne la version anglaise de la même date.

Les problèmes xml ont été résolus. Le source est à présent propre et l'évolution classique du livre peut reprendre son cours sans qu'il soit besoin de bricoler. Voilà une bonne chose de réglée.