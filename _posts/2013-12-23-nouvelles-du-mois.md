---
layout: post
title: Nouvelles du mois
date: 2013-12-23
tags: BLFS
---

Statistiques du mois :
- 119 commits traduits
- Les paquets suivants ont été mis à jour :
  - Passage à gimp-2.8.10
  - Passage à poppler-0.24.4
  - Passage à qemu-1.7.0
  - Passage à raptor2-2.0.11
  - Passage à xterm-298
  - Passage à xf86-input-wacom-0.23.0
  - Passage à libxshmfence-1.1
  - Passage à MesaLib-9.2.4
  - Passage à libpcap-1.5.1
  - Passage à subversion-1.8.5
  - Passage à check-0.9.11
  - Passage à gtk+-3.10.5
  - Passage à dovecot-2.2.9
  - Passage à httpd-2.4.7
  - Passage à samba-4.1.2
  - Passage à brasero-3.10.0
  - Passage à ImageMagick-6.8.7-7
  - Passage à libdrm-2.4.49
  - Passage à xproto-7.0.25
  - Passage à xscreensaver-5.23
  - Passage à freetype-2.5.1
  - Passage à gnome-desktop-3.10.2
  - Passage à mariadb-10.0.6
  - Passage à gnutls-3.2.7.
  - Passage à xorg-server-1.14.5
  - Passage à php-5.5.7
  - Passage à MesaLib-10.0.1
  - Passage à glproto-1.4.17
  - Passage à seamonkey-2.23
  - Passage à libfm-1.1.4
  - Passage à xscreensaver-5.26
  - Passage à Epiphany-3.10.3
  - Passage à qt-5.2.0
  - Passage à gdb-7.6.2
  - Passage à Archive::Zip-1.34
  - Passage à VLC-2.1.2
  - Passage à Colord-1.0.5
  - Passage à xf86-input-synaptics-1.7.2
  - Passage à Thunderbird-24.2.0
  - Passage à firefox-26.0
  - Passage à NSS-3.15.3.1
  - Passage à Gparted-0.17.0
  - Passage à samba-4.1.3
  - Passage à Lua-5.2.3
  - Passage à FreeType-2.5.2
  - Passage à libvpx-v1.3.0
  - Passage à KDE-4.11.4
  - Passage à phonon-4.7.1, phonon-backend-gstreamer-4.7.1 et phonon-backend-vlc-0.7.1
  - Passage à akonadi-1.11.0.
  - Passage à sqlite-3.8.2
  - Passage à gst-libav-1.2.1.
  - Passage à parole-0.5.4.
  - Passage à harfbuzz-0.9.25.
  - Passage à xf86-video-mga-1.6.3.
  - Passage à gtk+-3.10.6.
  - Passage à cups-filters-1.0.42.
  - Passage à libdrm-2.4.50.
  - Passage à WebKitGTK+-2.2.3.
  - Passage à libpcap-1.5.2.
  - Passage à xterm-300.
  - Passage à util-macros-1.18.0.
  - Passage à unrar-5.0.14.
  - Passage à Mercurial-2.8.1
  - Passage à git-1.8.5.1.
  - Passage à boost-1.55.0.
  - Passage à mc-4.8.11.
  - Passage à xterm-299.
  - Passage à gc-7.4.0.
  - Passage à libatomic_ops-7.2e
  - Passage à gnumeric-1.12.9.
  - Passage à goffice-0.10.9.

- 4 Paquets ajoutés :
  - liblinear-1.94 : une bibliothèque pour apprendre les classifieurs linéaires
  - SWIG-2.0.11 : un compilateur
  - impleburn-1.6.5 : programme minimaliste pour la gravure de CD et DVD
  - gnome-screenshot -3.10.1 : (retour des archives) : pour faire des captures d'écran

Denis a également proposé deux correctifs au livre BLFS pour corriger des liens cassés. Les deux correctifs ont été publiés.

La version en ligne sur le site est la version svn-r12393 du 14 décembre 2013.
