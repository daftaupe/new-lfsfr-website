---
layout: post
title: LFS 8.4 est disponible
date: 2019-03-01
tags: LFS
---

Nous sommes heureux de vous annoncer la sortie de LFS 8.4 conjointement en
version SysV et Systemd, ainsi que la sortie de BLFS 8.4 dans ces deux
versions.

Cette nouvelle version contient une mise à jour majeure de la chaîne d’outils
avec les versions glibc-2.29, binutils-2.32 et bash-5.0. Au total, ce sont 33
paquets qui ont été mis à jour. Un énorme travail rédactionnel a été réalisé
pour améliorer les textes tout au long du livre. Enfin, le noyau Linux a été
mis à jour vers la version 4.20.12.

Un grand merci à celles et ceux qui ont contribué à la traduction et aux
relectures !

N'hésitez pas à lire et télécharger le livre sur le site
[fr.linuxfromscratch.org](//fr.linuxfromscratch.org) aux formats html, pdf ou
epub, puis à nous adresser vos retours sur la liste de diffusion, le forum,
le canal IRC ou mastodon.

Bonne lecture !
