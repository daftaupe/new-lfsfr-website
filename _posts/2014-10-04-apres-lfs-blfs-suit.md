---
layout: post
title: Après LFS, BLFS suit
date: 2014-10-04
tags: BLFS
---

Nous sommes heureux de vous annoncer la publication de BLFS-7.6 en français moins d'une semaine après la version anglaise.

Beyond Linux From Scratch (BLFS), ou Au-delà de LFS, est le livre à lire après avoir construit LFS. En effet, si LFS se propose de vous guider dans l'installation d'un système très basique, BLFS vous donne la possibilité d'installer environ 750 paquets.

En utilisant BLFS 7.6, vous êtes certain que l'ensemble des instructions
données dans le livre fonctionne avec une LFS 7.6. Cela peut paraître
normal, il faut savoir qu'en l'espace d'environ 2 semaines, la petite équipe des rédacteurs de la version anglaise a testé les 750 paquets environ pour valider ces instructions.

Il est possible de transformer sa LFS en serveur LAMP, en serveur de fichiers, en PC de bureau avec XFCE, et il est possible en se basant sur BLFS de construire un OS simplifié avec seulement les fonctions que vous utilisez.

Tous les paquets sont présentés dans leur dernière version stable publiée.

Le devise de [B]LFS, "votre système, vos règles", résume à mon sens la philosophie du projet.

Si vous souhaitez des renseignements, de l'aide, en discuter, vous pouvez venir sur le salon IRC (#lfs-fr sur irc.freenode.net). Comme d'habitude, toute contribution à la relecture est bienvenue.

Nous remercions très chaleureusement Denis Mugnier, coordinateur de la traduction du livre, qui a déployé une énergie importante pour atteindre ce résultat, tant dans la conception des outils de gestion d'un tel projet que dans le travail de traduction proprement dit.

Enfin, merci également à l'association traduc.org, qui soutient le projet
en l'hébergeant.