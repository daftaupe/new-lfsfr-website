---
layout: post
title: Réservation possible des fichiers pour LFS-5.1.1
date: 2004-09-11
tags: LFS
---

Le site a été corrigé en profondeur suite au changement d'URL. J'en ai profité pour mettre à jour les tableaux de relecture pour la dernière version traduite de LFS.

La relecture peut donc réellement commencer. Il vous suffit de m'envoyer par courrier électronique le nom des fichiers que vous souhaitez relire et je vous les enverrais en réponse.