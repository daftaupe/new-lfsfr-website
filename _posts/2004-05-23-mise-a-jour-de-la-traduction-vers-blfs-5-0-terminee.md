---
layout: post
title: Mise à jour de la traduction vers BLFS-5.0 terminée
date: 2004-05-23
tags: BLFS
---

Voici enfin la version 5.0 de BLFS entièrement traduite. L'étape de relecture peut donc commencer. N'hésitez pas à me contacter pour vous lancer dans cette aventure :)

Je vais commencer à regarder les modifications apportées à la dernière version de LFS. Un coup de main sur ce point est aussi le bienvenu.