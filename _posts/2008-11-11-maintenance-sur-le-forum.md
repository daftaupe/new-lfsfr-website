---
layout: post
title: Maintenance sur le forum
date: 2008-11-11
tags: MAIN
---

Comme vous le savez, le forum de lfs est hébergé sur un site extérieur, pour des questions de commodités techniques. Dans quelques minutes, le forum sera momentanément indisponible pour cause de maintenance de ce site. Une mise à jour est nécessaire pour améliorer ses fonctionnalités et sa sécurité.

Nous vous tiendrons informés de la fin des opérations de maintenance. Le canal irc (irc.linuxfromscratch.org #lfs-fr) et la liste de diffusion restent à votre disposition pour tout besoin d'aide.

Merci de votre compréhension.