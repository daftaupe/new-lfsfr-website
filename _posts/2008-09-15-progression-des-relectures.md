---
layout: post
title: Progression des relectures
date: 2008-09-15
tags: MAIN
---

Grâce à notre contributeur principal, le projet LFS progresse. D'abord la traduction de la LFS SVN anglaise est terminée, la relecture étant finie. Les futures évolutions seront celles de LFS original, et d'éventuelles petites corrections occasionnelles et mineures.

LFS 6.3 français est en relecture active grâce à un nouveau contributeur. On peut espérer une sortie définitive d'ici quelques semaines ou mois.

Reste à relire la HLFS. On vient d'achever le débogage de ces livres, qui sont à présent potentiellement lisibles en html après compilation des sources xml. Reste une relecture de fond à faire. Nous recherchons quelqu'un pour cela.

BLFS 6.3 est en cours de traduction. La traducton de la CLFS stable sera bientôt entamée, s'agissant des livres pour x86_64 et pure64.

Pour BLFS svn, BLFS 6.3 (le traducteur actuel ne pourra pas tout traduire seul et il faudra ensuite la relire), HLFS à relire et CLFS, toute contribution est bienvenue. C'est l'une des conditions d'avancée rapide du projet. Une fois ces tâches accomplies, il n'y aura qu'à suivre les mises à jour et le travail sera moins ample. L'effort initial et actuel en vaut la peine.

Pour BLFS, vous pouvez nous envoyer des paquets du livre anglais pour lesquels vous aimeriez une traduction des explications de compilation. Nous les mettrons ainsi en priorité dans nos traductions de la 6.3 et/ou la version SVN anglaise.

N'hésitez pas à dialoguer avec nous via la liste de diffusion, le forum, l'irc, le mail perso.