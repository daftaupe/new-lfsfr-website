---
layout: post
title: BLFS 7.10 est là !
date: 2016-10-23
tags: BLFS
---

Nous sommes heureux de vous annoncer la sortie de BLFS 7.10 conjointement en version SysV et Systemd. Cela a été rendu possible principalement grâce à la fusion en amont des deux variantes

Cette sortie signifie également que nous comblons notre retard rapidement, et que nous nous concentrons maintenant sur la version de développement.

Un grand merci à celles et ceux qui ont contribué à la traduction, en particulier à Alexandre et Jean-Philippe.

Suite au changement de système de traduction, il est possible que certaines parties du livre ne soient pas affichées correctement. Si vous trouvez la moindre erreur, n'hésitez surtout pas à nous en faire part, que ce soit sur IRC, sur le forum ou sur les listes de diffusion.