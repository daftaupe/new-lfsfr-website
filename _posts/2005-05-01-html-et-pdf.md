---
layout: post
title: HTML et PDF
date: 2005-05-01
tags: MAIN
---

Ceux qui suivent mon blog ont pu s'apercevoir que je travaillais de nouveau sur la génération du PDF. Mais cette fois-ci, au lieu d'utiliser htmldoc, je tente le coup avec le système mis en place par LFS. Pour cela, j'utilise xslt roc (qui génère déjà la version HTML), puis fop pour la conversion en PDF.

Nous utilisons donc maintenant le système complet de génération. La ve sion HTML dispose des feuilles de style de la version originale, la version PDF est tout aussi jolie et utilisable que la version originale.