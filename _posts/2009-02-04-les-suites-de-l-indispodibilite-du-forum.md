---
layout: post
title: Les suites de l'indispodibilité du forum
date: 2009-02-04
tags: MAIN
---

absolinux mettant du temps à revenir, nous leur avons demandé des explications. Leur réponse est concluante et nous jugens utile de vous la faire partager.

Suite à quatre mails d'avertissement en une journée, l'hébergeur du site absolinux, la société 1and1, a tout simplement décidé de suspendre le site et la base ftp, au motif que des spams seraient envoyés depuis les adresses mail du site. Sans chercher à comprendre, sans essayer d'aider l'équipe qui développe absolinux, sans même leur laisser un délai raisonnable de 24 heures ni les moyens de consulter les logs, la sanction est tombée. Leurs messages nous ont été d'ailleurs transmis par l'équipe absolinux et nous ont stupéfaits par leur caractère robotique et leur mauvais français. On trouve par exemple des formules du type "nous regrettons que vous n'aurez pas" ou "vous ne saurez vous adresser au problème".

Non seulement leur sanction est injuste car sans appel ni soutien, mais les fautes de français la rendent tout simplement non crédible, ce qui est regrettable vu que cette société propose un service payant. Il n'est au surplus pas établi que le problème dénoncé n'émane pas de la structure même de 1and1, le bogue suggéré hâtivement étant inexistant.

Suite à cet événement scandaleux, le site absolinux s'organise. Il est en cours d'installation sur un serveur provisoir, offert généreusement par un de ses partenaires. On peut espérer un fonctionnement normal d'ici le 5 février. absolinux envisage d'ici une semaine d'exporter ensuite définitivement le site sur un serveur dédié et non plus sur un serveur partagé et géré par des gens irrespectueux et non crédibles. Ces perspectives devraient permettre  au forum d'être définitivement stable dès demain, puisque le serveur provisoir puis celui définitif seront gérés directement par l'équipe absolinux. Enfin, la migration prévue dans une semaine s'effectuera de manière transparente. Nous espérons donc le retour à la stabilité quasi définitive.

En souhaitant bon courage à absolinux, nous tenons ici à vous rassurer en vous annonçant le retour fonctionnel du forum sous peu, à  nous excuser de la gêne occasionnée indépendamment de la volonté d'absolinux et de la nôtre; mais nous en profitons pour vous informer des faits pour que vous soyez prévenus des conséquences éventuelles d'une souscription à cette société.