---
layout: post
title: Ouverture d'un forum
date: 2008-08-14
tags: MAIN
---

Pour mieux vous aider et pour faciliter la coordination, un forum vient d'être ouvert. Il suffit de vous y inscrire pour discuter de la traduction ou demander de l'aide dans le cadre de la mise en pratique des instructions d'un des livres traduits ici.

Pour en savoir plus, consultez la rubrique
Support.

La reprise active de la liste de diffusion est à l'étude. Nous vous tiendrons informé de l'évolution de la situation.