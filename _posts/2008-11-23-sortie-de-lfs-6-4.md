---
layout: post
title: Sortie de LFS 6.4
date: 2008-11-23
tags: LFS
---

La communauté Linux From Scratch et son équipe de traduction sont heureux de LFS version 6.4. Cette version comprend de nombreux changements par rapport à LFS-6.3 (y compris le passage à Linux-2.6.27,4, GCC-4.3.2, Glibc-2.8) et des corrections de sécurité. Elle comprend aussi un travail éditorial sur les explications dans le livres visant à améliorer la clarté et la précision du texte.
Pour une liste des changements, voir quoi de neuf.

Vous pouvez lire le livre ici ou le télécharger pour le lire hors connexion ici.