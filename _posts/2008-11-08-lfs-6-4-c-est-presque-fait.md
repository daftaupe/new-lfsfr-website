---
layout: post
title: LFS 6.4, c'est presque fait
date: 2008-11-08
tags: LFS
---

L'équipe francophone de LFS est ravie de vous annocer, avec deux jours de retard, la sortie de la traduction de la version 6.4-rc1 de LFS. Nos amis anglophones ont appelé cette version rc1 pour indiquer qu'elle est PRESQUE stable. Elle est dans sa phase terminale.

C'est donc une double bonne nouvelle. D'abord la traduction colle aux évolutions du texte original, si bien qu'on aura une stable pratiquement en même temps que les anglais. Ensuite, cette version annonce une 6.4 stable et définitive dans très peu de temps. À titre d'illustration, deux à trois mois se sont écoulés entre LFS-6.3rc1 et LFS-6.3. La LFS-6.3rc1 par rapport à la 6.3 n'a du reste pas subi d'énormes changements.

Vous pouvez donc dès à présent tester cette nouvelle version, qui présente l'avantage d'expliquer dans un livre stable la construction d'un système LFS avec des paquets dernier cri. Par exemple, on utili´;e le noyau 2.6.27.4, E2fsprogs qui contient Ext4, ...

N'hésitez pas à nous transmettre des retours de test. On aidera ainsi les anglophones à définitivement stabiliser cette version dans sa derniöre phase de test.

On l'attendait depuis un an. Ici on l'annonçait depuis septembre. C'est chose faite. Bon test à tous.